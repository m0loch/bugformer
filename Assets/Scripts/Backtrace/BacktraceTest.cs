using Backtrace.Unity;
using Backtrace.Unity.Model;
using System;
using System.Collections.Generic;
using UnityEngine;

public class BacktraceTest : MonoBehaviour
{
    [SerializeField] private BacktraceClient backtraceClient;

    private void Start()
    {
        try
        {
            throw new Exception();
        }
        catch (Exception exception)
        {
            var report = new BacktraceReport(
               exception: exception,
               attributes: new Dictionary<string, string>() { { "key", "value" } },
               attachmentPaths: new List<string>() { @"file_path_1", @"file_path_2" }
           );

           backtraceClient.Send(report);
        }
    }
}
